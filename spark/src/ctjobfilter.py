
import re

def industryfilter(industry):
    industry = industry.replace('\n',',')
    return industry

def categoryfilter(title):
    iOS = re.findall(r'iOS',title)
    IOS = re.findall(r'IOS',title)
    UI = re.findall(r'UI',title)
    UX = re.findall(r'UX',title)
    AI = re.findall(r'AI',title)
    title = title.lower()
    try:
        director = re.findall(r'director',title)
        manager = re.findall(r'manager',title)
        supervisor = re.findall(r'supervisor',title)
        head = re.findall(r'head',title)
        lead = re.findall(r'lead',title)
        if len(manager)>=1 or len(supervisor)>=1 or len(head)>=1 or len(lead)>=1 or len(director)>=1:
            category = "manager"
            return category 

        devops = re.findall(r'devops',title)
        if len(devops) >= 1:
            category = 'DevOps engineer'
            return category

        test = re.findall(r'test',title)
        if len(test) >= 1:
            category = "tester" 
            return category
        system = re.findall(r'system',title)
        engineer = re.findall(r'engineer',title)

        if len(engineer)>= 1:
            data = re.findall(r'data',title)
            network = re.findall(r'network',title)
            if len(data)>=1:
                category = "data engineer"
            elif len(network)>=1:
                category = "network engineer"
            elif len(system)>=1:
                category = "system engineer"
            else:
                category = "programmer"
            return category

        analyst = re.findall(r'analyst',title)
        if len(analyst) >= 1:
            technical = re.findall(r'technical',title)
            if len(technical) >= 1:
                category = "technical analyst"
                return category
            
            programmer = re.findall(r'programmer',title)
            if len(system) >= 1 or len(programmer)>=1:
                category = "system analyst"
                return category
            business = re.findall(r'business',title)
            data = re.findall(r'data',title)
            if len(business) >= 1 or len(data):
                category = "data analyst"
                return category
        cloud = re.findall(r'cloud',title)
        if len(cloud) >= 1:
            category = "cloud service"
            return category

        database = re.findall(r'database',title)
        if len(database) >= 1:
            category = "database"
            return category
        
        blockchain = re.findall(r'blockchain',title)
        crypto = re.findall(r'crypto',title)
        if len(blockchain)>= 1 or len(crypto):
            category = "blockchain"
            return category

        security = re.findall(r'security',title)
        if len(security)>=1:
            category = "cyber security"
            return category

        scientist = re.findall(r'scientist',title)
        data = re.findall(r'data',title)
        
        machine = re.findall(r'machine',title)
        learn = re.findall(r'learn',title)
        if (len(scientist)>=1 and len(data)>=1)or (len(AI)>=1)or (len(machine)>=1 and len(learn)>=1):
            category = "data scientist"
            return category

        business = re.findall(r'business',title)
        intelligen = re.findall(r'intelligen',title)
        analytic = re.findall(r'analytic',title)
        if(len(business)>=1 and len(intelligen)>=1)or (len(data)>=1 and len(analytic)>=1) :
            category = "data analyst"
            return category

        web = re.findall(r'web',title)
        mobile = re.findall(r'mobile',title)
        android = re.findall(r'android',title)
        front = re.findall(r'front',title)
        back =  re.findall(r'back',title)
        end = re.findall(r'end',title)  
        if (len(front)>=1 and len(end)>=1)or len(UI)>=1 or len(UX)>=1:
            category = "front-end developer"
            return category
        if(len(back)>=1 and len(end)>=1):
            category = "back-end developer"
            return category
        if len(iOS)>=1 or len(IOS)>=1 or len(mobile)>=1 or len(android)>=1:
            category = "mobile application developer"
            return category
        if len(web)>=1 :     
            category = "web developer"
            return category
                
        support = re.findall(r'support',title)
        asistance = re.findall(r'assistance',title)
        asistant = re.findall(r'assistant',title)
        technician = re.findall(r'technician',title)
        if len(support) >= 1 or len(asistant) >= 1 or len(asistance) or len(technician) >= 1:
            category = "IT support"
            return category
        developer = re.findall(r'developer',title)
        programmer = re.findall(r'programmer',title)
        if len(developer)>=1 or len(programmer)>=1:
            category ="programmer"
    except: 
        pass
    category = title
    return category
#key words
# manager,supervisor,head,lead                   > manager!
# test                                           > tester!
# network #engineer#                             > network engineer!
# system #analyst#                               > system analyst!
# devops                                         > DevOps engineer!
# technical #analyst #                           > technical analyst  !
# data #engineer#                                > data engineer!
# #data analyst#,business intelligence,#business analyst#,data analytic> data analyst!
# #data scientist#,machine learning,ai             > data scientist!
# cloud                                          > cloud service!
# database                                       > database !
# support,asistance,asistant,technician          > IT support !
# mobile,android,ios                             > mobile developer!
# web,front-end,frontend,UI/UX,back-end,backend  > web developer!
# programmer,#engineer#,developer                > programmer
# security                                       > cyber security!
# blockchain,crypto                              > blockchain related!

def locationfilter(location):
    try:
        location1 = re.findall(r'Chek Lap Kok',location)
        if location1[0] == "Chek Lap Kok":
            location = "Chek Lap Kok"
            return location
    except:
        pass
    text = re.findall(r' ',location)
    if len(text) >= 3:
        print(text)
        location = ''
    return location

def datefilter(date):
    try:
        inputdate = re.findall(r'\d+',date)
        date = int(inputdate[2])
    except:
        return ''
    return date

def monthfilter(date):
    try:
        inputmonth = re.findall(r'\d+',date)
        month = int(inputmonth[1])
    except:
        return ''
    return month
def yearfilter(date):
    try:
        inputyear = re.findall(r'\d+',date)
        year = int(inputyear[0])
    except:
        return ''
    return year
    
def meansalaryfilter(salary):
    if salary == '':
        return salary
    try:
        salaryrange = salary.replace(',','').replace(' ','').replace('/','').replace('month','')
        salaryrange = re.findall(r'\d\d\d\d\d', salaryrange)
        if len(salaryrange) <= 1:
            return ""
        inputsalary = 0
        for x in salaryrange:
            inputsalary = int(x) + inputsalary
        inputsalary = int(inputsalary/2)
    except:
        return ''
    return inputsalary

def lowersalaryfilter(salary):
    if salary == '':
        return salary
    try:
        salaryrange = salary.replace(',','').replace(' ','').replace('/','').replace('month','')
        salaryrange = re.findall(r'\d\d\d\d\d', salaryrange)
        if len(salaryrange) <= 1:
            return ""
        inputsalary = 100000
        for x in salaryrange:
            y = int(x)
            if y < inputsalary:
                inputsalary = y
    except:
        return ''
    return inputsalary

def uppersalaryfilter(salary):
    if salary == '':
        return salary
    
    salaryrange = salary.replace(',','').replace(' ','').replace('/','').replace('month','')
    salaryrange = re.findall(r'\d\d\d\d\d', salaryrange)
    if len(salaryrange) <= 1:
        return ""
    inputsalary = 0
    for x in salaryrange:
        y = int(x)
        if y > inputsalary:
            inputsalary = y
    return inputsalary


def experiencefilter(experience):
    if experience == '':
        return experience
    exrange = re.findall(r'\d+',experience)
    input = ''
    y = 100
    for x in exrange:
        if int(x) <= y:
            y = int(x)
            input = y
    return input

def workingdayfilter(benefits):
    try:
        output = re.findall(r'\d',benefits)
        workingday = output[0]
    except:
        return''
    return workingday

def jobtypefilter(jobtype):
    try:
        jobtype = jobtype.lower()
        output1 = re.findall(r'permanent',jobtype)
        output2 = re.findall(r'full',jobtype)
        output3 = re.findall(r'part',jobtype)
        output4 = re.findall(r'contract',jobtype)
        if len(output1) >= 1 or len(output2)>= 1:
            jobtype = "Full-time"
        elif len(output3)>=1:
            jobtype = "Part-time"
        elif len(output4)>=1:
            jobtype = "Contract"
    except:
        return ''
    return jobtype
if __name__ == '__main__': 
    input =  "Contract"
    output = jobtypefilter(input)
    print(output)
    # job = {'key1':'abc','key2':'bcd'}
    # del job['key1']
    # print(job)
    pass