# from matplotlib.pyplot import title
from selenium import webdriver
from selenium.webdriver.chrome.service import Service
from webdriver_manager.chrome import ChromeDriverManager
from selenium.webdriver.chrome.options import Options
from selenium.webdriver.common.by import By
# from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.common.action_chains import ActionChains
from selenium.webdriver.common.keys import Keys
import time
from sseclient import SSEClient as EventSource
import json
from pymongo import MongoClient
import datetime
import os
import time
from dotenv import load_dotenv
load_dotenv()
from pyspark.sql import SparkSession
from pyspark.sql.types import StructType,StructField, StringType
import shutil 
import ctjobfilter


SPARK_ADDRESS=os.getenv('SPARK_ADDRESS')

def setup():
    global client
    client = MongoClient('mongodb', 27017)
    global db
    db = client.ctjob
    chrome_options = Options()
    chrome_options.add_argument("--headless")
    chrome_options.add_argument("--no-sandbox")
    chrome_options.add_argument('--disable-dev-shm-usage')
    global driver
    driver = webdriver.Chrome(options=chrome_options,service=Service(ChromeDriverManager().install()))
    driver.set_window_size(1600, 7000)
    # driver.get("https://ctgoodjobs.hk")
    driver.get("https://www.ctgoodjobs.hk/jobs/information-technology")
    global actions
    actions = ActionChains(driver)
    time.sleep(10)
    # search = driver.find_element(By.ID,"searchb-job-func-m")
    # actions.click(search)
    # actions.perform()
    # time.sleep(8)
    # search = driver.find_element(by=By.XPATH,value='//*[@id="searchb-cb-job-func-021"]')
    # actions.click(search)
    # actions.perform()
    # time.sleep(5)
    # search = driver.find_element(By.ID,'search-keyword-m')
    # search.send_keys("")
    # search.send_keys(Keys.RETURN)
    # time.sleep(10)

def scraplink():
    global linklist
    for page in range(2,145):
        try:
            linklist = []
            for list in range(1,32):
                try:
                    trial = driver.find_element(by=By.XPATH,value=f'/html/body/section/main/aside/div/div[2]/div[1]/div/div[1]/div[{list}]')
                    trialclass = trial.get_attribute("class")
                    if trialclass == "row jl-row jl-de active" or trialclass == "row jl-row jl-de" or trialclass == "row jl-row jl-pc":
                        trialdivlink = driver.find_element(by=By.XPATH,value=f'/html/body/section/main/aside/div/div[2]/div[1]/div/div[1]/div[{list}]/div[1]/h2/a')
                        link= trialdivlink.get_attribute("href")
                        linklist.append(link)
                except Exception as e: 
                    print(e)
                    pass
            now = int(time.time())
            if not os.path.exists('../tmp/streaming'):
                os.makedirs('../tmp/streaming')
            with open('../tmp/streaming/links-{}.csv'.format(now),'w') as f:
                for row in linklist:
                    f.write(row+"\n")        
            driver.execute_script("var q=document.documentElement.scrollTop=10000")
            time.sleep(1)
            nextpage = driver.find_element_by_link_text(f'{page}')
            actions.click(nextpage)
            actions.perform()
            print(linklist)
            print(f'page : {page}') 
        except Exception as e: 
            print(e)
            pass
def scrap():
    spark = SparkSession.\
            builder.\
            appName('links').\
            getOrCreate()
    df = spark.read.csv('../tmp/streaming/links-*.csv',
                    header=False,
                    inferSchema=True)
    x = 1
    for row in df.rdd.collect():
            try:
                job={
                    'title': '',   
                    'link': '',
                    'location': '',  
                    'company': '',
                    'experience': '', 
                    'job_type': '',
                    'industries':'',
                    'uppersalary':'',
                    'lowersalary':'',
                    'meansalary': '',
                    'salaryrange':'',
                    'workingday':'',  
                    'date':'',
                    'month':'',
                    'year':'',
                    # 'career level':''
                    'category':'',
                    'count':'1'
                }
                driver.execute_script(f"window.open('{row[0]}');")
                list_handle = driver.current_window_handle
                handles = driver.window_handles 
                new_handle = None
                for handle in handles:
                    if handle != list_handle:
                        new_handle = handle
                driver.switch_to.window(new_handle)
                try:
                    time.sleep(6)
                    temp = driver.find_element_by_css_selector('div.col-sm-8.job-ref').text
                    job['date'] = ctjobfilter.datefilter(temp)
                    job['month']= ctjobfilter.monthfilter(temp)
                    job['year']= ctjobfilter.yearfilter(temp)
                    try:
                        tempvalue = driver.find_element_by_css_selector('div.col-sm-6.post-date').text
                        if tempvalue != '':
                            job['date'] = ctjobfilter.datefilter(tempvalue)
                            job['month']= ctjobfilter.monthfilter(tempvalue)
                            job['year']= ctjobfilter.yearfilter(tempvalue)
                    except:
                        pass
                    job['title'] = driver.find_element_by_css_selector('h1.job-title').text
                    job['link'] = row[0]
                    job['company'] = driver.find_element(By.ID,'company_name').text
                    driver.execute_script("var q=document.documentElement.scrollTop=10000")
                    try:
                        for i in range(1,9):
                            trial2div = driver.find_element(by=By.XPATH,value=f'//tr[{i}]/td[1]')
                            trial2div2 = driver.find_element(by=By.XPATH,value=f'//tr[{i}]/td[2]')
                            if trial2div.text == 'Salary':
                                if trial2div2.text== 'N/A ( Search your salary info in )':
                                    pass
                                else:
                                    job['salaryrange']=trial2div2.text
                                    job['meansalary']= ctjobfilter.meansalaryfilter(job['salaryrange'])
                                    job['uppersalary']= ctjobfilter.uppersalaryfilter(job['salaryrange'])
                                    job['lowersalary']= ctjobfilter.lowersalaryfilter(job['salaryrange'])
                            elif trial2div.text == 'Experience':
                                job['experience']=ctjobfilter.experiencefilter(trial2div2.text)
                            elif trial2div.text == 'Location':
                                job['location']=ctjobfilter.locationfilter(trial2div2.text)
                            elif trial2div.text == 'Industry':
                                job['industries']=ctjobfilter.industryfilter(trial2div2.text)
                            elif trial2div.text == 'Employment Term':
                                job['job_type'] = ctjobfilter.jobtypefilter(trial2div2.text)
                            elif trial2div.text == 'Benefits':
                                job['workingday'] = ctjobfilter.workingdayfilter(trial2div2.text)
                            # elif trial2div.text == 'Career Level':
                            #     job['career level'] = trial2div2.text
                    except Exception as e: 
                        pass            
                except Exception as e: 
                    pass
                try:
                    del job['salaryrange']
                except:
                    pass
                job['category']= ctjobfilter.categoryfilter(job['title'])
                driver.close()
                time.sleep(1)
                driver.switch_to.window(list_handle)
                print(f"!Number{x}:{job}")
                x=x+1
                db.ctjob.insert_one(job)
            except Exception as e: 
                print(e)
                pass


def quitchrome():
    shutil.rmtree('../tmp/streaming')

    driver.quit()


if __name__ == '__main__':
    setup()
    scraplink()
    scrap()
    quitchrome()
    