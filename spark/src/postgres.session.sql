psql -U postgres
CREATE DATABASE job;

CREATE TABLE staging_table(
   ID SERIAL PRIMARY KEY,
   title          VARCHAR(255),
   link           TEXT,
   experience     VARCHAR(255),
   company        VARCHAR(255),
   uppersalary    VARCHAR(255),
   lowersalary    VARCHAR(255),
   meansalary     VARCHAR(255),
   date           VARCHAR(255),
   month          VARCHAR(255),
   year           VARCHAR(255),
   location       VARCHAR(255),
   category       VARCHAR(255),
   industries     VARCHAR(255),
   job_type       VARCHAR(255),
   workingday     INT,
   count          INT,
   created_at TIMESTAMP DEFAULT NOW()
);

CREATE TABLE fact_job_table(
   ID SERIAL PRIMARY KEY,
   title          VARCHAR(255),
   link           TEXT,
   company        VARCHAR(255),
   date_id        INT,
   foreign key (date_id) references dim_date(ID),
   category_id    INT,
   foreign key (category_id) references dim_category(ID),
   industries_id  INT,
   foreign key (industries_id) references dim_industries(ID),
   job_type_id    INT,
   foreign key (job_type_id) references dim_job_type(ID),
   workingday_id  INT,
   foreign key (workingday_id) references dim_workingday(ID),
   experience_id  INT,
   foreign key (experience_id) references dim_experience(ID),
   location_id    INT,
   foreign key (location_id) references dim_location(ID),
   salary_id      INT,
   foreign key (salary_id) references fact_salary(ID),
   count          INT,
   created_at     TIMESTAMP DEFAULT NOW()
);

CREATE TABLE dim_date(
   ID         SERIAL PRIMARY KEY,
   year       VARCHAR(255),
   month      VARCHAR(255),
   date        VARCHAR(255),
   created_at TIMESTAMP DEFAULT NOW(),
   updated_at TIMESTAMP DEFAULT NOW()
);

CREATE TABLE dim_category(
   ID         SERIAL PRIMARY KEY,
   category         VARCHAR(255),
   created_at TIMESTAMP DEFAULT NOW(),
   updated_at TIMESTAMP DEFAULT NOW()
);

CREATE TABLE fact_salary(
   ID SERIAL PRIMARY KEY,
   meansalary   VARCHAR(255),
   lowersalary  VARCHAR(255),
   uppersalary  VARCHAR(255),
   created_at TIMESTAMP DEFAULT NOW(),
   updated_at TIMESTAMP DEFAULT NOW()
);

CREATE TABLE dim_location(
   ID SERIAL PRIMARY KEY,
   location VARCHAR(255),
   created_at TIMESTAMP DEFAULT NOW(),
   updated_at TIMESTAMP DEFAULT NOW()
);

CREATE TABLE dim_industries(
   ID SERIAL PRIMARY KEY,
   industries VARCHAR(255),
   created_at TIMESTAMP DEFAULT NOW(),
   updated_at TIMESTAMP DEFAULT NOW()
);

CREATE TABLE dim_experience(
   ID SERIAL PRIMARY KEY,
   experience VARCHAR(255),
   created_at TIMESTAMP DEFAULT NOW(),
   updated_at TIMESTAMP DEFAULT NOW()
);

CREATE TABLE dim_job_type(
   ID SERIAL PRIMARY KEY,
   job_type VARCHAR(255),
   created_at TIMESTAMP DEFAULT NOW(),
   updated_at TIMESTAMP DEFAULT NOW() 
);

CREATE TABLE dim_workingday(
   ID SERIAL PRIMARY KEY,
   workingday INT,
   created_at TIMESTAMP DEFAULT NOW(),
   updated_at TIMESTAMP DEFAULT NOW() 
);

CREATE UNIQUE INDEX date_unique_idx on dim_date (date,month,year);
CREATE UNIQUE INDEX category_unique_idx on dim_category (category);
CREATE UNIQUE INDEX industries_unique_idx on dim_industries (industries);
CREATE UNIQUE INDEX experience_unique_idx on dim_experience (experience);
CREATE UNIQUE INDEX location_unique_idx on dim_location (location);
CREATE UNIQUE INDEX salary_unique_idx on fact_salary (uppersalary,lowersalary,meansalary);
CREATE UNIQUE INDEX job_type_unique_idx on dim_workingday (workingday);
CREATE UNIQUE INDEX workingday_unique_idx on dim_job_type (job_type);

CREATE OR REPLACE FUNCTION insert_fact_table() RETURNS trigger AS $$
    DECLARE
        category_id     integer;
        date_id         integer;
        industries_id   integer;
        experience_id   integer;
        location_id     integer;
        salary_id       integer;
        workingday_id   integer;
        job_type_id     integer;

    BEGIN
         INSERT INTO dim_category (category) VALUES
            (NEW.category) ON CONFLICT(category) 
            DO UPDATE set updated_at = NOW() RETURNING ID into category_id;

         INSERT INTO dim_date (year,month,date) VALUES 
            (NEW.year,NEW.month,NEW.date) ON CONFLICT(year,month,date)
            DO UPDATE set updated_at = NOW() RETURNING ID into date_id;
         
         INSERT INTO dim_industries (industries) VALUES 
            (NEW.industries) ON CONFLICT(industries)
            DO UPDATE set updated_at = NOW() RETURNING ID into industries_id;
         
         INSERT INTO dim_experience (experience) VALUES 
            (NEW.experience) ON CONFLICT(experience)
            DO UPDATE set updated_at = NOW() RETURNING ID into experience_id;

         INSERT INTO dim_location (location) VALUES 
            (NEW.location) ON CONFLICT(location)
            DO UPDATE set updated_at = NOW() RETURNING ID into location_id;
         
         INSERT INTO fact_salary (uppersalary,lowersalary,meansalary) VALUES 
            (NEW.uppersalary,NEW.lowersalary,NEW.meansalary) ON CONFLICT(uppersalary,lowersalary,meansalary)
            DO UPDATE set updated_at = NOW() RETURNING ID into salary_id;

         INSERT INTO dim_workingday (workingday) VALUES 
            (NEW.workingday) ON CONFLICT(workingday)
            DO UPDATE set updated_at = NOW() RETURNING ID into workingday_id;

         INSERT INTO dim_job_type (job_type) VALUES 
            (NEW.job_type) ON CONFLICT(job_type)
            DO UPDATE set updated_at = NOW() RETURNING ID into job_type_id;

         INSERT INTO fact_job_table (title,link,company,count,
            date_id,category_id,location_id,industries_id,experience_id,salary_id,job_type_id,workingday_id) VALUES
            (NEW.title, NEW.link, NEW.company, NEW.count,
            date_id,category_id,location_id,industries_id,experience_id,salary_id,job_type_id,workingday_id);
        return NEW;
    END
$$ LANGUAGE plpgsql;

CREATE TRIGGER fact_table_trigger AFTER INSERT ON staging_table
FOR EACH ROW EXECUTE PROCEDURE insert_fact_table();
