from dotenv import load_dotenv
load_dotenv()
import os
from pyspark.sql import SparkSession
import pyspark.sql.functions as F

AWS_ACCESS_KEY = os.getenv('AWS_ACCESS_KEY')
AWS_SECRET_KEY = os.getenv('AWS_SECRET_KEY')

POSTGRES_DB=os.getenv('POSTGRES_DB')
POSTGRES_USER=os.getenv("POSTGRES_USER")
POSTGRES_PASSWORD=os.getenv("POSTGRES_PASSWORD")

packages = [
    "org.mongodb.spark:mongo-spark-connector_2.12:3.0.1",
    "org.postgresql:postgresql:42.2.18"
]
    
spark = SparkSession.builder.appName("Mongo To PSQL")\
        .master('spark://spark:7077')\
        .config("spark.jars.packages",",".join(packages))\
        .getOrCreate()

df = spark.read.format('mongo').option('spark.mongodb.input.uri','mongodb://mongodb/job').load()


df = df.drop('_id')


df.write.format('jdbc')\
    .option('url','jdbc:postgresql://postgres/job')\
    .option('dbtable','staging_table')\
    .option('user',POSTGRES_USER)\
    .option('password',POSTGRES_PASSWORD)\
    .option('driver','org.postgresql.Driver')\
    .mode('append')\
    .save()